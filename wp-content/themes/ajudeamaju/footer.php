<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ajudeamaju
 */
global $configuracao;
?>

	<!-- RODAPÉ -->
	<footer class="rodape">
		<div class="socialNetworks">
			<ul>
				<?php if($configuracao['opt_rs_facebook']): ?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_facebook'] ?>">
						<i class="fab fa-facebook-f"></i>
						<span>Facebook</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_twitter']): ?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_twitter'] ?>">
						<i class="fab fa-twitter"></i>
						<span>Twitter</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_google']): ?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_google'] ?>">
						<i class="fab fa-google-plus-g"></i>
						<span>Google+</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_instagram']): ?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_instagram'] ?>">
						<i class="fab fa-instagram"></i>
						<span>Instagram</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_pinterest']): ?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_pinterest'] ?>">
						<i class="fab fa-pinterest-p"></i>
						<span>Pinterest</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_youtube']):?>
				<li>
					<a href="<?php echo $configuracao['opt_rs_youtube'] ?>">
						<i class="fab fa-youtube"></i>
						<span>Youtube</span>
					</a>
				</li>
				<?php endif; if($configuracao['opt_rs_email']): ?>
				<li>
					<a href="mailto:<?php echo $configuracao['opt_rs_email'] ?>">
						<i class="far fa-envelope"></i>
						<span>E-mail</span>
					</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>
		<?php if($configuracao['opt_rodape_texto_copyright']): ?>
		<div class="copyright">
			<p><?php echo $configuracao['opt_rodape_texto_copyright']; ?></p>
		</div>
		<?php endif; ?>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
